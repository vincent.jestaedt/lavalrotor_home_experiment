import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_massage_gun.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# die UUID des Beschleunigungssensors
acceleration_sensor_uuid = '1ee847be-fddd-6ee4-892a-68c4555b0981'

# Erstellen des Hauptdictionaries für den Sensor
acceleration_data_dict = {
    acceleration_sensor_uuid: {
        'acceleration_x': [],
        'acceleration_y': [],
        'acceleration_z': [],
        'timestamp': []
    }
}

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Erstellen der Verbindung zum Beschleunigungssensor
i2c = board.I2C()
accelerometer = adafruit_adxl34x.ADXL345(i2c)

# Zeitpunkt des Startens der Messung
startzeitpunkt = time.time()

# Schleife für die Messung
while time.time() - startzeitpunkt <= measure_duration_in_s:
    # Messdaten erfassen
    acceleration_data_dict[acceleration_sensor_uuid]['acceleration_x'].append(accelerometer.acceleration[0])
    acceleration_data_dict[acceleration_sensor_uuid]['acceleration_y'].append(accelerometer.acceleration[1])
    acceleration_data_dict[acceleration_sensor_uuid]['acceleration_z'].append(accelerometer.acceleration[2])
    acceleration_data_dict[acceleration_sensor_uuid]['timestamp'].append(time.time() - startzeitpunkt)

    # 1 ms pausieren
    time.sleep(0.001)

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Speichern des Dictionaries in einer HDF5-Datei

# Attribute für die Einheiten
unit_attributes = {
    'acceleration_x': 'm/s^2',
    'acceleration_y': 'm/s^2',
    'acceleration_z': 'm/s^2',
    'timestamp': 's'
}

# Öffnen der HDF5-Datei im Modus 'a' (append)
with h5py.File(path_h5_file, 'a') as hdf_file:
    # Überprüfen, ob die UUID des Sensors bereits im HDF5-File vorhanden ist
    if acceleration_sensor_uuid not in hdf_file:
        # Sensorgruppe erstellen
        sensor_group = hdf_file.create_group(acceleration_sensor_uuid)

        # Datenreihen hinzufügen
        for measurement, values in acceleration_data_dict[acceleration_sensor_uuid].items():
            # Datensatz erstellen
            dataset = sensor_group.create_dataset(measurement, data=values)

            # Attribut für Einheit hinzufügen
            dataset.attrs['unit'] = unit_attributes[measurement] 
            print(f'{measurement}: {values} {unit_attributes[measurement]}') 
# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
