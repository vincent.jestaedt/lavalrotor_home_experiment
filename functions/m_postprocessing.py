"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple
from scipy.interpolate import interp1d
#from scipy.fft import fft
import matplotlib.pyplot as plt

def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    # Berechnen des Betrags der Beschleunigung
    vec_magnitude = np.sqrt(x**2 + y**2 + z**2)
    return vec_magnitude
    

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    # Linear interpolieren
    interpolate_function = interp1d(time, data, kind='linear', fill_value='extrapolate')

    # Neue, äquidistante Zeitpunkte erstellen
    new_time = np.linspace(min(time), max(time), len(time))

    # Interpolation der Daten für die neuen Zeitpunkte durchführen
    interpolated_data = interpolate_function(new_time)

    return new_time, interpolated_data


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    
    # Mittelwert subtrahieren
    x -= np.mean(x)
    
    # Berechnung der FFT
    n = len(time)
    fft_result = np.fft.fft(x)
    
    # Berechnung der Frequenzen
    sampling_frequency = 1 / np.mean(np.diff(time))
    frequencies = np.fft.fftfreq(n, d=1/sampling_frequency)
    
    # Nur die positiven Frequenzen betrachten (Einseitige FFT)
    positive_frequencies = frequencies[:n//2]
    
    # Berechnung der Amplitude (Betrag) der FFT
    amplitude = np.abs(fft_result[:n//2])
  
    return amplitude, positive_frequencies

